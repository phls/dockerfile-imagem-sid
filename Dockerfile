FROM base-sid:20191219

ARG DEBIAN_FRONTEND=noninteractive

COPY *.txt /tmp/
COPY update-copyright.in /root/
COPY .gitconfig /root/
COPY .vimrc /root/
ADD gnupg.tar.gz /root/
ADD ssh.tar.gz /root/
ADD vim.tar.gz /root/

RUN apt update \
    && apt dist-upgrade -y \
    && apt install -y autopkgtest blhc devscripts dh-make dput-ng git-buildpackage how-can-i-help mc quilt renameutils spell splitpatch tree rsync locales cdbs vim \
    && apt clean \
    && ln -fs /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime \
    && dpkg-reconfigure -f noninteractive tzdata \
    && bash -c 'mkdir /PKG' \
    && bash -c 'chown root:root /root/.gnupg -R' \
    && bash -c 'chown root:root /root/.ssh -R' \
    && bash -c 'chown root:root /root/.vim -R' \
    && bash -c 'cat /tmp/mod-apt.txt >> /etc/apt/sources.list' \
    && bash -c 'cat /tmp/mod-bash.txt >> /etc/bash.bashrc' \
    && bash -c 'cat /tmp/mod-devscripts.txt >> /etc/devscripts.conf' \
    && bash -c 'cat /tmp/mod-gbp.txt >> /etc/git-buildpackage/gbp.conf' \
    && bash -c 'cat /tmp/mod-lintianrc.txt >> /etc/lintianrc' \
    && bash -c 'cat /tmp/mod-vim.txt >> /etc/vim/vimrc' \
    && rm /tmp/*.txt

!/bin/bash

image="jaula-sid"

#get timestamp for the tag
timestamp=$(date +%Y%m%d)

tag=$image:$timestamp

#build image
docker build -t $tag .

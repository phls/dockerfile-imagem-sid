
# PERSONALIZACOES
alias ls='ls --color=auto'
alias tree='tree -aC'
alias debuildsa='dpkg-buildpackage -sa -k0443C450'
alias uscan-check='uscan --verbose --report'
export DEBFULLNAME="Paulo Henrique de Lima Santana (phls)"
export DEBEMAIL="phls@debian.org"
export EDITOR=vim
export LANG=C.UTF-8
export LANGUAGE=C.UTF-8
export LC_ALL=C.UTF-8
export QUILT_PATCHES=debian/patches
export PS1='JAULASID-\u@\h:\w\$ '

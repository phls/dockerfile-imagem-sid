set number
set spell spelllang=en_us
syntax enable
colorscheme monokai
inoremap <F2> <CR><C-R>=repeat(' ',col([line('.')-1,'$'])-col('.'))<CR><C-O>:.retab<CR>
